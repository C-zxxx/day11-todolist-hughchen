import request from "./request";

export const loadTodos = () => {
  return request.get('todos')
}

export const toggleTodo = (id, done) => {
  return request.put(`todos/${id}`, {
    done,
  })
}

export const addTodo = (item) => {
  return request.post('todos', item)
}


export const deleteTodo = (id) => {
  return request.delete(`todos/${id}`)
}

export const editTodo = (id, text) => {
  return request.put(`todos/${id}`, {
    text,
  })
}