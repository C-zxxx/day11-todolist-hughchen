import { message } from "antd";
import axios from "axios";

const request = axios.create({
  baseURL: 'https://64c0c5630d8e251fd11288e0.mockapi.io/',

})

request.interceptors.response.use(
  (response) => response,
  (error) => {
    console.log("error.response.data",error.response.data);
    const msg = error.response.data
    if (msg) {
      message.error(msg) 
    }else{
      message.error("请求错误~")
    }
    return Promise.reject(error)
  }
)

export default request