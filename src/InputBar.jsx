import { useState } from "react"
import "./InputBar.css"
import { useDispatch } from "react-redux"
import { initTodos } from "./todoSlice"
import * as api from "./apis/todo"
import { useTodo } from "./hooks/useTodo"
import { Button } from "antd"

const InputBar = () => {

  const dispatch = useDispatch()
  const [newItem, setNewItem] = useState("")
  const PRESS_ENTER = 13
  const { reloadTodos } = useTodo()
  const [buttonLoading, setButtonLoading] = useState(false)



  function onClick() {
    setButtonLoading(true)
    if (newItem.trim() === "") {
      setNewItem("")
      alert("Please input something~")
      setButtonLoading(false)
      return
    }

    api.addTodo({
      id: new Date().getTime(),
      text: newItem,
      done: false,
    }).then(res => {
      reloadTodos()
      setButtonLoading(false)
    }).catch(err => {
      
    })

    setNewItem("")
  }

  function pressEnter(e) {
    if (e.keyCode === PRESS_ENTER) {
      onClick(e)
    }
  }

  function changeInputValue(e) {
    setNewItem(e.target.value)
  }

  return (
    <div className="inputBar">
      <input type="text" value={newItem} placeholder="Please input your Todo Item" onChange={changeInputValue} onKeyUp={pressEnter}></input>
      <Button type="primary" onClick={onClick} loading={buttonLoading}>ADD</Button>
    </div>
  )
}
export default InputBar