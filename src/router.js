import * as React from "react";
import { createBrowserRouter } from "react-router-dom";
import TodoList from "./TodoList";
import AboutPage from "./pages/AboutPage";
import DoneList from "./pages/todo/DoneList.jsx";
import Layout from "./components/Layout.jsx";
import TodoDetail from "./pages/todo/TodoDetail";
import NotFoundPage from "./pages/NotFoundPage";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children:[
      {
        path: "/",
        element: <TodoList />,
      },
      {
        path: "/done",
        element: <DoneList />,
      },
      {
        path: "/about",
        element: <AboutPage />,
      },
      {
        path: "todo/:id",
        element:<TodoDetail />
      },
      {
        path: "*",
        element:<NotFoundPage />
      }
    ]
  },
 
]);
