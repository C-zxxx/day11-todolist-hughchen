import ToDoItem from './ToDoItem';
import InputBar from './InputBar';
import "./TodoList.css"
import { useDispatch, useSelector } from 'react-redux';
import * as React from "react";
import { useEffect } from 'react';
import { useTodo } from './hooks/useTodo';

function TodoList() {
  const todoList = useSelector(state => state.todo.todoList)
  const { reloadTodos } = useTodo()

  useEffect(() => {
    reloadTodos()
  }, [])

  return (
    <div className='todoList'>
      <div className='title'>Todo List</div>
      <div className="todoItems">
        {todoList.map((item, index) => {
          return <ToDoItem item={item} key={index} index={index} />
        })}
      </div>
      <InputBar />
    </div>
  );
}

export default TodoList;
