import { useDispatch } from "react-redux"
import { onChangeStatus, onDeleteIcon, onSaveEdit } from "./todoSlice"
import "./ToDoItem.css"
import { useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import * as api from "./apis/todo"

const ToDoItem = (props) => {
  const dispatch = useDispatch()
  const [editItem, setEditItem] = useState(false)
  const [editText, setEditText] = useState("")
  const navigate = useNavigate()
  const inDoneList = useLocation().pathname === "/done"

  function changeStatus() {
    if (!inDoneList) {
      api.toggleTodo(props.item.id, !props.item.done).then(res => {
        dispatch(onChangeStatus(props.item.id))
      }).catch(err => {
      
      })
    } else {
      navigate(`/todo/${props.item.id}`)
    }
  }

  function deleteItem() {
    api.deleteTodo(props.item.id).then((res) => {
      dispatch(onDeleteIcon(props.item.id))
    }).catch(err => {
      
    })
  }

  function onEditItemClick() {
    setEditItem(true)
    setEditText(props.item.text)
  }

  function changeEditText(e) {
    setEditText(e.target.value)
  }

  function onSaveClick() {
    if (editText.trim() === "") {
      alert("Please input something~")
      return
    }


    api.editTodo(props.item.id, editText).then(res => {
      setEditItem(false)
      dispatch(onSaveEdit({
        id: props.item.id,
        newText: editText
      }))
    }).catch(err => {
      
    })
  }

  function onCancelClick() {
    setEditItem(false)
  }


  return (
    <div className={!props.item.done ? "todoItem" : "done todoItem"} >
      {
        !editItem ? (
          <>
            <div className={!props.item.done ? "text" : "textDone text"} onClick={changeStatus}>{props.item.text}</div>
            {
              !inDoneList && (
                <>
                  <img src={require("./asset/edit.png")} alt="" onClick={onEditItemClick} />
                </>
              )
            }
            <img src={require("./asset/delete.png")} alt="" onClick={deleteItem} />

          </>
        ) : (
          <>
            <input className="editInput" type="text" value={editText} onChange={changeEditText}></input>
            <img src={require("./asset/save.png")} alt="" onClick={onSaveClick} />
            <img src={require("./asset/cancel.png")} alt="" onClick={onCancelClick} />
          </>
        )
      }
    </div>
  )
}

export default ToDoItem