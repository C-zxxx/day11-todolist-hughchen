import { useDispatch } from "react-redux"
import { loadTodos } from "../apis/todo"
import { initTodos } from "../todoSlice"

export const useTodo = () => {
  const dispatch = useDispatch()

  const reloadTodos = () => {
    loadTodos().then(res => {
      dispatch(initTodos(res.data))
    }).catch(err =>{
      // console.log(err);
    })
  }

  return {
    reloadTodos
  }

}
