import ToDoItem from '../../ToDoItem';
import { useSelector } from 'react-redux';
import * as React from "react";
import "./DoneList.css"

const DoneList = () => {

  const todoList = useSelector(state => state.todo.todoList)
  
  const doneList = todoList.filter(item => {
    return item.done
  })

  return (
    <div className='doneList'>
      <div className='title'>Done List</div>
      {doneList.map((item, index) => {
        return <ToDoItem item={item} key={index} index={index} />
      })}
    </div>
  );
}

export default DoneList;