
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";

const TodoDetail = () => {
  const navigate = useNavigate()
  const params = useParams()
  const [searchParams] = useSearchParams()
  
  const id = params.id
  const todoList = useSelector(state => state.todo.todoList)

  const todoItem = todoList.find(element => {
    return element.id.toString() === id
  });

  useEffect(() => {
    if (!todoItem) {
      navigate("/404")
    }
  }, [todoItem, navigate])

  return (
    <div>
      <h1> Detail </h1>

      <div>
        {todoItem && todoItem.text}
      </div>

    </div>
  );
}

export default TodoDetail;