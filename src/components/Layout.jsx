import { Outlet } from "react-router-dom";
import { NavLink } from "react-router-dom";
import "./Layout.css"
const Layout = () => {
  return (
    <div className="layout">
      <header>
        <NavLink className="link" to="/">TODOLIST</NavLink>
        <NavLink className="link" to="/done">DONELIST</NavLink>
        <NavLink className="link" to="/about">ABOUT</NavLink>
      </header>
      <Outlet></Outlet>
    </div>
  );
}

export default Layout;